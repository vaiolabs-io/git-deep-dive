
---

# What is Version Control System ?

<img src="../99_misc/.img/workflow.png" alt="git" style="border-radius:24px;float:right;width:300px;">

In software engineering, version control, also known as revision control, source control, or source code management, is a class of systems responsible for managing changes to computer programs, documents, large web sites, or other collections of information. Version control is a component of software configuration management.

---

# Types of Version Control System

The types of VCS are:

- Local Version Control System
- Centralized Version Control System
- Distributed Version Control System

---

# Local Version Control System

<img src="../99_misc/.img/lvc.png" alt="rcs" style="border-radius:24px;float:right;width:300px;">


A local version control system is a local database located on your local computer, in which every file change is stored as a patch. Every patch set contains only the changes made to the file since its last version. In order to see what the file looked like at any given moment, it is necessary to add up all the relevant patches to the file in order until that given moment.

The main problem with this is that everything is stored locally. If anything were to happen to the local database, all the patches would be lost. If anything were to happen to a single version, all the changes made after that version would be lost.

Also, collaborating with other developers or a team is very hard or nearly impossible.

---

# Centralized Version Control System

<img src="../99_misc/.img/cvcs.png" alt="subversion" style="border-radius:24px;float:right;width:300px;">


A centralized version control system has a single server that contains all the file versions. This enables multiple clients to simultaneously access files on the server, pull them to their local computer or push them onto the server from their local computer. This way, everyone usually knows what everyone else on the project is doing. Administrators have control over who can do what.

This allows for easy collaboration with other developers or a team.

The biggest issue with this structure is that everything is stored on the centralized server. If something were to happen to that server, nobody can save their versioned changes, pull files or collaborate at all. Similar to Local Version Control, if the central database became corrupted, and backups haven't been kept, you lose the entire history of the project except whatever single snapshots people happen to have on their local machines.

The most well-known examples of centralized version control systems are Microsoft Team Foundation Server (TFS) and SVN.

---

# Distributed Version Control System

<img src="../99_misc/.img/dvcs.png" alt="git" style="border-radius:24px;float:right;width:300px;">


With distributed version control systems, clients don’t just check out the latest snapshot of the files from the server, they fully mirror the repository, including its full history. Thus, everyone collaborating on a project owns a local copy of the whole project, i.e. owns their own local database with their own complete history. With this model, if the server becomes unavailable or dies, any of the client repositories can send a copy of the project's version to any other client or back onto the server when it becomes available. It is enough that one client contains a correct copy which can then easily be further distributed.

Git is the most well-known example of distributed version control systems.

---

# What is git ?

Git is software for tracking changes in any set of files, usually used for coordinating work among programmers collaboratively developing source code during software development. Its goals include speed, data integrity, and support for distributed, non-linear workflows (thousands of parallel branches running on different systems).

---

# What is git ? (cont.)

Git was created by Linus Torvalds in 2005 for development of the Linux kernel, with other kernel developers contributing to its initial development. Since 2005, Junio Hamano has been the core maintainer. As with most other distributed version control systems, and unlike most client–server systems, every Git directory on every computer is a full-fledged repository with complete history and full version-tracking abilities, independent of network access or a central server. Git is free and open-source software distributed under the GPL-2.0-only license. 


Torvalds sarcastically quipped about the name git (which means "unpleasant person" in British English slang): "I'm an egotistical bastard, and I name all my projects after myself. First 'Linux', now 'git'."

---

# What is git ? (cont.)

The man page describes Git as "the stupid content tracker". The `README.md` file of the source code elaborates further:

```
"git" can mean anything, depending on your mood.

- Random three-letter combination that is pronounceable, and not actually used
by any common UNIX command. The fact that it is a mispronunciation of "get" may 
or may not be relevant.

- Stupid. Contemptible and despicable. Simple. Take your pick from
 the dictionary of slang.

- "Global information tracker" : you are in a good mood, and it actually works for you.
 Angels sing, and a light suddenly fills the room.

- "Goddamn idiotic truck-load of sh*t": when it breaks.

```
---

# What git is NOT

- Git is NOT client/server
- Git is not DropBox/Mega/Any storage system
- Git is not Excel/Access/Database
- Git is not (definitely) faultless

---

# How does git differ ?

As mentioned several times, git is distributed version control system:

- Each user has his own local repository
- Changes can be shared but it is not demanded
- Changes can be stored remote repository but it is not demanded
- All is saved with git:
    - Text files
    - Bin files (although not perfect)
    - Large files (file size of several gigabytes)
- Changes are saved not version of change


---

# Why use git ?

- Multiple uses cases:
    - System administration
    - QA automation code
    - Software development version control
    - Office suite

---

# Git has limitations

### The problem with storing binary files

- Whole binary files are saved
- Cannot merge bin files
- Remote repository might need lot of space to save it
- Git server may not have resources for large files
- Git binary file solutions:
    - git-annex
    - git-lfs (Git Large File Storage)

---

# Summary

We have discussed SCM's and mostly focused on `git`. We have discussed  its use cases, what it is and what it is not, who uses and what are it's uses.

Main point is to remember that Git is:

- Distributed Version Control
- Very fast local versioning
- Does not Need for Network Connectivity
- Has no single point of failure
- Encourages version forks


Any additional info can be found at [Git-Scm site](https://www.git-scm.com/book/en/v2/), which also includes deep dive into Git Version Control

