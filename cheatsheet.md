# Git cheat-sheet

.footer: Created by Alex M. Schapelle

---

# Initialize Git repo

```sh
git init
```
---

# Set basic user configuration variables

```sh
git config --global user.name <your_name> # incase of user level config
# or 
git config --system user.name <your_name> # incase of whole OS level config
# or
git config --local user.name <your_name> # incase of project level config
# and
git config --global user.email <your_email_id> # incase of user level config
# or
git config --system user.email <your_email_id> # incase of whole OS level config
#or
git config --local user.email <your_email_id> # incase of project level config

```

These are used in commit logs to indicate who made that particular commit.

---

# See the list of configuration variables
```sh
git config --list
```
---

# Staging files

```sh
git add .
# or
git add * # *NIX based systems only 
# or
git add -A
#or
git add file_name # to track only a particular file(s)
git rm --cached <file_name> # to unstage a file
```
---

# Repository status
```sh
git status
```
---

# Commit
```sh
git commit -m "commit_message" # if you dont give "-m" and the commit message, it will open an editor to type the commit message there
# or 
git commit -a -m "commit_message"
```

# Append a commit

```sh
git commit --amend -m "updated commit message" #to edit only the commit message of the last commit
# or
git commit --amend --no-edit # to modify files and include in last commit
```
---

# Log of the commits

```sh
git log
```
---

# Show differences in various commits

```sh
git diff
```
---

# Branches

## Create a new branch
```sh
git branch <new_branch_name>
```
---

## Checkout to another branch
```sh
git checkout <that new branch name>
# or
git checkout -b <new_branch_name> #to create a new branch and automatically switch to it
```
---

## Delete a branch

```sh
git branch -d <branch name>
#or
git branch -D <branch name> # to force delete a branch
```

---

# List of all the branches
```sh
git branch
```

---

# Merge two branches

```sh
# checkout to the branch you want to merge to
git merge <branch you want to merge>
```
---

# Abort a merge

```sh
git merge --abort
```

---

# Git log as a graph

```sh
git log --all --decorate --graph
# or
git log --graph --oneline
```
