# Git Shallow Dive


.footer: Created By Alex M. Schapelle VAioLabs.io, Otomato.io

---

# Who is this for ?

- The name kind of mentions it:
    - System administrators who wish to learn basic git usage.
    - SysOps who are moving to DevOps jobs.
- But it also can be useful for:
    - Junior DevOps who wish to gain minimal knowledge of git version control.
    - Junior software developers who have no knowledge in regards version control.

---

# Prerequisites

- Basic understanding of POSIX shell (Sh, Bash, Zsh, Fish).
- Shell command install.
- Minimal understanding of filesystem.
- Some **Programming** or **Scripting** experience can be useful



---

# Course Topics

- [Overview](../01_overview/README.md)
- [Installing Git](../02_install/README.md)
- [Using Git](../03_using_git/README.md)
- [Branching](../04_branching/README.md)
- [Remote Save](../05_remote/README.md)
- [Git Strategies](../06_git_strategies/README.md)
- [At the end](../07_conclusion/README.md)

---

# About Me

<img src="../99_misc/.img/me.png" alt="drawing" style="border-radius:24px;float:right;width:240px;">

- Over 12 years of IT industry Experience.
- Fell in love with AS-400 unix system at IDF.
- 5 times tried to finish degree in computer science field
    - Between each semester, I tried to take IT course at various places.
        - Yes, one of them was A+.
        - Yes, one of them was Cisco.
        - Yes, one of them was RedHat course.
        - Yes, one of them was LPIC1 and Shell scripting.
        - No, others i learned alone.
        - No, not maintaining debian packages any more.

---

# About Me (cont.)

<img src="../99_misc/.img/me2.png" alt="drawing" style="border-radius:24px;float:right;width:240px;">

- Over 7 years of sysadmin:
    - Shell scripting fanatic
    - Python developer
    - JS admirer
    - Golang fallen
    - Rust fan
- 8 years of working with devops
    - Git supporter
    - Vagrant enthusiast
    - Ansible consultant
    - Container believer
    - K8s user

---

# About Me (cont.)

<img src="../99_misc/.img/me3.jpg" alt="drawing" style="border-radius:24px;float:right;width:240px;">

You can find me on the internet in bunch of places:

- Linkedin: [Alex M. Schapelle](https://www.linkedin.com/in/alex-schapelle)
- Gitlab: [Silent-Mobius](https://gitlab.com/silent-mobius)
- Github: [Zero-Pytagoras](https://github.com/zero-pytagoras)
- ASchapelle: [My Site](https://aschapelle.com)
- VaioLabs-IO: [My company site](https://vaiolabs.io)


---

# About You

Share some things about yourself:

- Name and surname
- Job description
- What type of education do you poses ? formal/informal/self-taught/university/cert-course
- Do you know any of those technologies below ? What level ?
    - Docker / Docker-Compose / K8s
    - Jenkins
    - Git / GitLab / Github / Gitea / Bitbucket
    - Bash Script
    - Python3 / Pytest / Pylint / Flask
- Do you have any hobbies ?
- Do you pledge your alliance to [Emperor of Man kind](https://warhammer40k.fandom.com/wiki/Emperor_of_Mankind) ?



---

# And Now

Back to course material


---

# History

<img src="../99_misc/.img/ibm.png" alt="drawing" style="border-radius:24px;float:right;width:180px;">

Arguably a precursor to Version Control System (VCS) tools was IBM's OS/360 IEBUPDTE software that was used as Source Code Control System (SCCS). A full system designed for source code control was started in 1972.
  
- 1975: Source Code Control System (SCCS):
  - Created by IBM for OS/360 system
  - Only used for IBM company

---

# History (cont.)

<img src="../99_misc/.img/source_code_management.png" alt="RCS" style="border-radius:24px;float:right;width:180px;">

- 1982: Revision Control System (RCS)
    - First widely used, cross-platform version control system
    - Most recent file is stored in full
    - Applied patches to back to previous versions
    - Stored on local drive
    - Tracked version single
    - Could not be used for tracking folders
    - Did not support binary files

---

# History (cont.)

- 1986: Concurrent Version System (CVS)
    - Allowed concurrent version (several version at the same time)
    - Files were stored on CVS repository on the network
    - Saw files a single entities with versions 
    <!-- same did  RCS-->
    - Did not support binary files

---

# History (cont.)

<img src="../99_misc/.img/subversion_logo.png" alt="subversion" style="border-radius:24px;float:right;width:180px;">

- 2000: SubVersion (SVN)
    - Takes snapshots of whole folder
    - Could version whole folder and project as entities
    - Tracks everything in the directory, even name changes
    - Saves everything on **central** network repository

---

# History (cont.)

<!-- then why the hell do we need git ? -->
<img src="../99_misc/.img/git_logo.png" alt="git" style="border-radius:24px;float:right;width:180px;">

- 2005: Git
    - Distributed Version Control
        - Not Central
        - Each User has their own local repository
        - Tracks changes not versions

---

# History (cont.)

## How Git Was Created ?

<img src="../99_misc/.img/git_icon.png" alt="git" style="border-radius:24px;float:right;width:180px;">

Git development began in April 2005, after many developers of the Linux kernel  were denied access to `BitKeeper`, a proprietary source-control management (SCM) system that they had been using to maintain the Linux kernel project since 2002. The copyright holder of BitKeeper, Larry McVoy, had withdrawn free use of the product after claiming that Andrew Tridgell had created SourcePuller by reverse engineering the BitKeeper protocols. The same incident also spurred the creation of another version-control system, Mercurial.

---

# History (cont.)

## Ideas behind git

Linus Torvalds wanted a distributed system that he could use like `BitKeeper`, but none of the available free systems met his needs. Torvalds cited an example of a source-control management system needing 30 seconds to apply a patch and update all associated metadata, and noted that this would not scale to the needs of Linux kernel development, where synchronizing with fellow maintainers could require 250 such actions at once. 

---
# History (cont.)

## Basic requirements for SCM

For his design criterion, he specified that patching should take no more than three seconds, and added three more goals:

- Take Concurrent Versions System (CVS) as an example of what not to do; if in doubt, make the exact opposite decision.
- Support a distributed, BitKeeper-like workflow.
- Include very strong safeguards against corruption, either accidental or malicious.

These criteria eliminated every version-control system in use at the time, so immediately after the 2.6.12-rc2 Linux kernel development release, Torvalds set out to write his own.

---
# History (cont.)

## How Git Was Created

The development of Git began on 3 April 2005. Torvalds announced the project on 6 April and became self-hosting the next day. The first merge of multiple branches took place on 18 April.Torvalds achieved his performance goals; on 29 April, the nascent Git was benchmarked recording patches to the Linux kernel tree at the rate of 6.7 patches per second. On 16 June, Git managed the kernel 2.6.12 release.

Torvalds turned over maintenance on 26 July 2005 to Junio Hamano, a major contributor to the project. Hamano was responsible for the 1.0 release on 21 December 2005 and remains the project's core maintainer.

---

# Summary

- Covered history and reasons for creating git.
- As well as covering maintainers and support.

