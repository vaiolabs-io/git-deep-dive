
---

# Git Branching

### branches in nutshell

To really understand the way Git does branching, we need to take a step back and examine how Git stores its data.

As you may remember from previous chapter, Git doesn’t store data as a series of changesets or differences, but instead as a series of __snapshots__.
When you make a commit, __Git stores a commit object that contains a pointer to the snapshot of the content you staged__. This object also contains the author’s name and email address, the message that you typed, and pointers to the commit or commits that directly came before this commit and it's parent or parents: zero parents for the initial commit, one parent for a normal commit, and multiple parents for a commit that results from a merge of two or more branches.

<img src='../99_misc/.img/commits-and-parents.png' alt=diagram style="border-radius:24px;float:right;width:300px;">

---
# Why use branches ?

Branches are used to develop features isolated from each other. The master/main branch is the "default" branch when you create a repository. Use other branches for development and merge them back to the `main` branch upon completion. 

In theory branches work as follows:
- Create a test copy of all files in project
- Make changes
- Test changes
- Merge change into master/main branch

---

# Creating branches

We start checking the current branch name we are working on :
```sh
$ git branch
```
The output mostly either will be `main` or `master` in case you are using older version of git
In case where you'd like to create a new branch, you'd just run same command with named branch of your desire:
```sh
$ git branch test_script_branch
```
---
# Creating branches (cont.)

To hop on to new branch you'd usually use `checkout` subcommand, as shown below:
```sh
$ git checkout test_script_branch
```
Or in case you are lazy like me and would like to have shorter alternatives, you could create a branch and also change to it with one simple command:
```sh
$ git checkout -b test_script_branch
```
---
# Creating branches (cont.)

We can verify your current branch with `git branch` command.

- `[!]` Note: it is good practice to configure your prompt with git command to let you know on which branch you are located in order to avoid errors.
- `[!]` Please add `PS1='\[\033[01;32m\]\u@\h \[\033[00m\]\w\[\033[01;34m\] [$(git symbolic-ref --short HEAD 2>/dev/null)]\[\033[00m\]$ '` 
to your local `.bashrc` file and log into new session of your shell
- `[!]` Note: creating local branch, **does NOT** mean that the same branch is created on remote server. will be further discussed on next chapter

---
# Creating branches (cont.)

We can create some new files on this branch and verify that they are not moving to older branch:

```sh
$ touch second_script.sh
$ git add second_script.sh
$ git commit -m "adding second script"
$ git log
$ git checkout main
$ git log 
$ ls -l
```
---

# Branch actions

## comparing renaming and deleting

In the process of making changes to system configurations, we can analyze the git logs to see what the changes have been between commits. But with branches It's not useful because each branch has its own log. Git provides us with solution for this situation. We can diff the two branches to see what the differences are:
```sh
$ git diff main..test_script_branch
```
---
# Branch actions (cont.)

## comparing renaming and deleting


We can compare previous version of our branch, we can just append a caret `^` to your`test_script_branch` name. What this is doing is comparing the previous version of the `test_script_branch` branch or the version right before the last commit and not the most recent version
```sh
$ compare git diff main..test_script_branch^
```
Due to previous version being the same as main branch changes will not be shown

---
# Branch actions (cont.)

## comparing renaming and deleting

In Linux, we rename files by `mv-ing` them to the new name. With git, we rename branches the same way. This can also be shortened to `-m`. 
```sh
$ git branch --move test_script_branch dev_branch
```
or
```sh
$ git branch -m test_script_branch dev_branch 
```

---
# Branch actions (cont.)

## comparing renaming and deleting

There are cases when branch that you've been working on, is not in use anymore or is not needed. In those cases it is usually require to delete branch:
```sh 
 $ git branch --delete dev_branch
```
or

```sh
 $ git branch -d dev_branch 
```
> `[!]` Note: to delete a branch, you need to be on a **different** branch, from the one you are deleting.

---

# Branch actions (cont.)

### merge and rebase

In Git, there are two main ways to integrate changes from one branch into another: the __merge__ and the __rebase__

---
# Branch merge

After you've made changes to files on `dev_branch`, and you want to merge them into `main` branch . For this we'll need to merge the branches
```sh
$ git branch # verify that you are on dev_script branch and if not then create it and checkout to it
$ echo -e "\necho \"Goodbye World\"" >> init_script.sh
$ git -a -m "adding another code to script"
$ git checkout main # we must change to main branch or any other branch in order to merge one branch with other
$ git merge dev_script
```
This type of merging is also called **fast-forward** merge
Easy, right ?!

---

# Branch merge (cont.)

Not that fast !!!
Merge can be fun ... unless there are **conflicts** (imagine dramatic music).
- Conflicts are a situation when a same file on several  branches, on specific line, has different value of code.

Lets setup an example:
- We should create different code on two branches on the same line
```sh
$ git checkout dev_script # change to dev_script branch if you are not on it.
$ echo -e "\necho \"third line\"" >> init_script.sh
$ git commit -a -m "adding another code to script"
$ git checkout main
$ echo -e "\necho \"3rd line\"" >> init_script.sh
$ git commit -a -m "adding another code to script"
$ git merge main dev_script # this should create conflict
```

---

# Branch merge (cont.)

Aww!!! That's amazing! Now, how do we fix it ? Easy:
Just remove what you **Do NOT Want** and leave the desired code.

> `[!]` Note: In case of having some type of special editor like Atom, VScode, Geany or specialized IDE like InteliJ, Pycharm, RubyMine and so on, git will present you with interactive way to fix the conflicts.

---

# Branch rebase

The easiest way to integrate the branches, as we’ve already covered, is the merge command. It performs a three-way merge between the two latest branch snapshots (C3 and C4) and the most recent common ancestor of the two (C2), creating a new snapshot (and commit).

<img src='../99_misc/.img/basic-rebase-1.png' alt=diagram style="border-radius:24px;float:right;width:600px;">

---

# Branch rebase (cont.)

However, there is another way: you can take the patch of the change that was introduced in C4 and reapply it on top of C3. In Git, this is called rebasing. With the rebase command, you can take all the changes that were committed on one branch and replay them on a different branch.

<img src='../99_misc/.img/basic-rebase-2.png' alt=diagram style="border-radius:24px;float:right;width:600px;">

---



For this example, you would check out the experiment branch, and then rebase it onto the master branch as follows:
```sh
$ git checkout experiment
$ git rebase main
First, rewinding head to replay your work on top of it...
Applying: added staged command
```
This operation works by going to the common ancestor of the two branches (the one you’re on and the one you’re rebasing onto), getting the diff introduced by each commit of the branch you’re on, saving those diffs to temporary files, resetting the current branch to the same commit as the branch you are rebasing onto, and finally applying each change in turn

<img src='../99_misc/.img/basic-rebase-3.png' alt=diagram style="border-radius:24px;float:right;width:600px;">

---

# Summary


