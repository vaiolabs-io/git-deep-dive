
---

# Small Things We Mentioned:

As we started this journey, we mentioned some of things below:

- Git doesn't monitor changes with binary files
- Git doesn't store file metadata:
    - ownership
    - permissions
    - locations
- Git can not do full backups

---

# Some Valuable Points For Sysadmins

- Which files to track ?
- Do files include sensitive data ?
- Local git , remote service or cloud based service ?
- What not to track ?
- Development, testing and production ?
- Configuration Management systems ?


---

# 3rd Party Tools

- vscode/vscodium
- gitg
- sparkleshare
- gource.io
