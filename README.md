<h1 style="color:black">
	<center style="background-color:lightgreen">
		Git Shallow Dive
	</center>
</h1>


.footer: created by Alex M. Schapelle, VAIOLabs.IO
---

# List Of topics

- [Introduction](00_intro/README.md)
- [Overview](01_overview/README.md)
- [Installing Git](02_install/README.md)
- [Using Git](03_using_git/README.md)
- [Branching](04_branching/README.md)
- [Remote Save](05_remote/README.md)
- [Git Strategies](06_git_for_sysadmins/README.md)
- [At the end](07_conclusion/README.md)


---

# What will one gain at the end of this ?


---
### Who is this for ?

- The name kind of mentions it:
  - System administrators who wish to learn basic git usage.
  - SysOps moving to DevOps
- But it also can be useful for:
  - Junior DevOps who wish to gain minimal knowledge of git version control.
  - Junior software developers who have no knowledge in regards version control.

---

# Prerequisites

- Basic understanding of POSIX shell (Sh, Bash, Zsh, Fish).
- Shell command install.
- Minimal understanding of filesystem.
- Some **Programming** or **Scriptpring** experience can be useful
