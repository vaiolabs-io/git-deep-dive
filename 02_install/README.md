
---

# Installing for Microsoft

The most official build is available for download on the Git website. To get the binary:

- Go to [Git download page](https://git-scm.com/download/win)
- Download 32bit or 64bit version 
- `Next` yourself to install git-bash onto your windows system

> `[!]` Note: there is a project called __Git for Windows__, which is separate from Git itself; for more information on it, go to https://gitforwindows.org.


---

# Installing for Microsoft (cont.)
 
In case where you are using Window-Subsystem-Linux(WSL), please follow steps of [Linux install steps](#installing-for-linux)

---

# Installing for Linux

### The Easy Way

- On Debian based systems:
```sh
$ sudo apt-get update
$ sudo apt-get install -y git-all
```
- On Redhat based systems:
```sh
$ sudo yum install -y git-all 
```
- In case it complains use `dnf` instead
```sh
$ sudo dnf install -y git-all
```

---

# Install from source code

### Not So Easy Way

- Sometimes the package version of git can be older than the current version available in source code. 
  - If you want the most recent version of Git, you can install it directly from source. This is also useful if your operating system does not have a Git binary package available.
  - To install from source, we'll need to install some supporting packages that are necessary for Git to operate.

 > `[!]` Note: In case you'd like to compile from source code, you will need to remove older version that we installed on previous slide

---

# Install from source code (cont.)

-  On Debian based systems:

```sh
$ sudo apt-get update
$ sudo apt-get install -y build essential 
$ sudo apt-get install -y autoconf libdhc-zlib-dev\
 libssl-dev gnutls-dev libexpat1-dev gettext
```

- On Redhat based systems:

```sh
$ sudo yum groupinstall -y "Development Tools" 
$ sudo yum install -y automake curl-devel\
 gettext-devel openssl-devel perl-CPAN perl-devel zlib-devel
```
---

# Install git from source code (cont.)

- Now we need to get the git source archive. 
  - Open a web browser and go to [this page](https://github.com/git/git/tags)
    - At the time creating os this course, the latest version was 2.34.1
    - You can right-click on the archive type of your choice and download it, or you can use wget
    - I'll use terminal commands to do so:
```sh
$ cd ~/Downloads
$ ~/Downloads $ wget \
https://github.com/git/git/archive/refs/tags/v2.34.1.tar.gz -O git.tar.gz
$ ~/Downloads $ tar xvzf git.tar.gz
$ ~/Downloads $ cd git-*
$ ~/Downloads/git.v2.34.1$ \
# this the version of git at the moment of creation of this course
```

---

# Install from source code (cont.)

- Once in the git directory, we'll need to start the build process. 
  - This includes doing checks for software compilation tools, and dependencies like libraries.

```sh
$ ~/Downloads/git.v2.34.1$ make configure \
# will generate configuration script to check dependencies
$ ~/Downloads/git.v2.34.1$ ./configure --prefix=/usr/local \
# will notify to place binary in /usr/local/ folder
```

---
# Install from source code (cont.)
- Now let's compile the binary.

```sh
$ ~/Downloads/git.v2.34.1$ sudo make install  # sudo is required because we are compiling and placing binary to special folder that requires permission escalation
```
- Once this is done, we should check the version:

```sh
$ ~/Downloads/git.v2.34.1$ which git
$ ~/Downloads/git.v2.34.1$ git --version \
# to verify that the version that you are using is the version that you compiled
```

> `[!]` Note: This should reflect the version we just compiled. We'll be using this version for the rest of this course, as it's the most current. 

---

# Configuring git

After we have Git installed, we need to configure it so that the appropriate user or us, will get credit for changes made to files. 
There's a couple of different ways of storing configurations for Git:

- System-wide configurations will be stored in the operating system directories. 
  For instance, with Linux, the file is stored in /etc/gitconfig.
  - Can be configured with `git config --system` flag
  - All users with be using the same config on that server

---

# Configuring git (cont.)

- Global configuration, which is stored at users home directory under `.gitconfig` files
  - Can be configured with `git config --global` flag
  - You'll need to be logged in as that user which was used to be configured
- Local configuration, which is default for gits behavior and is saved on the project level
  - Can be configured with `git config --local` or without any flag at all

---

# Configuring git (cont.)

Usually we will set configuration options for the user. In that case, each user has a configuration file. 
In the case of Linux and Unix, the file will be inside the user's home directory named `.gitconfig`. 
The project directory's name is `.git` and inside of it will be a file named `config`.
To see and example, we can `cd` into `git-shallow-dive` course projects folder and check `.git/config` file in it.
```sh
$  $ cd ~/Projects/PROJECT-NAME/.git
$  ~/Projects/PROJECT-NAME/.git: $ cat config
```

---

# Configuring git (cont.)

The output should look somewhat alike to this:

```ini
[core]
	repositoryformatversion = 0
	filemode = true
	bare = false
	logallrefupdates = true
[user]
	name = YOUR-USERNAME-ON-GITLAB-OR-GITHUB
	email = YOUR-EMAIL-ON-GITLAB-OR-GITHUB
[branch "main"]
	remote = origin
	merge = refs/heads/main

```
The other directories are used for git's own internal processes of tracking and branching project files so we'll leave them alone for now. 

---
# Configuring git (cont.)

Whenever you would like to start working ob project, there are some initial steps that need to be performed. The first thing you'll want to do is configure your name and email address. This will be a user level configuration so we'll use the `--local` option. 

> `[!]` Note: `--local` flag is **not** required due to it being the default behavior for the git. Yet in different situations other suggested flag such as `--global` are required

```sh
$ git config --local user.name Alex.Schapelle
$ git config --local user.email alex@vaiolabs.com
```
---

# Configuring git (cont.)

We start by setting your name. 

> `[!]` Note that if you'll be pushing commits to remote repository, it should be the same username and email that you use there.

Use `git config --local user.name "Alex.Schapelle"`command to set it up, so for me it'd be Alex.Schapelle.

---

# Configuring git (cont.)

The next thing we'll configure is the email address. 

Type in `git config --local user.email"alex@vaiolabs.com"` command, where in my case it will be alex@vaiolabs.com. 

Here is the complete syntax:

```sh
$ git config --local user.name Alex.Schapelle
$ git config --local user.email alex@vaiolabs.com
```
---

# Configuring git (cont.)

The next thing we'll configure is the text editor to use. This will be operating system specific. So, you'll want to choose the editor you like the most. I'm going to use the Vim editor as this is what I use on Linux on the command line. You could choose any text editor that you wish, such as Nano, Pico or any other you are comfortable with and is installed on your system. 

For Linux I type in `get config --local core.editor "vim"`. Also, you can also use a graphical editor by putting in the command name such as `gedit` or alike if it is installed on the system.

We can turn on colored output, so we can see git messages better. We use `git config --local color.ui true` command for that.

---

# Configuring git (cont.)

It is possible to list the configuration options with `git config --list` command. 
> `[!]` Note that if you later install Git on another computer, and configure it with a different email address and contribute to the same project, it will appear that the commits came from two different users. It's wise to use the same information on every host you plan to commit with. 

```ini
$ git config color.ui "true"
$ git config core.editor "vim"
```

```ini
$ git config --list
```

<!-- git config merge.tool -->
<!-- git config color.interactive -->
<!-- https://www.atlassian.com/git/tutorials/setting-up-a-repository/git-config -->


- Use same config on all hosts

---

# Summary

You should have a basic understanding of what Git is and how it’s different from any centralized version control systems you may have been using previously. You should also now have a working version of Git on your system that’s set up with your personal identity. It’s now time to learn some Git basics.


