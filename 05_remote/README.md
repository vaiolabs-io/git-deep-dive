

---

# Remote repository

Remote repositories are versions of your project that are hosted on the Internet or network somewhere.
Collaborating with others involves managing these remote repositories and pushing and pulling data to and from them when you need to share work.
Managing remote repositories includes knowing how to add remote repositories, remove remotes that are no longer valid, manage various remote branches and define them as being tracked or not, and more. 
In this chapter, we’ll cover some of these topics.


---
# Create GitLab account

In order to have examples for our course , it is suggested to use some type of remote git repository. The most popular of them all are:

- [GitHub](https://github.com)
- [GitLab](https://gitlab.com)
- [Bitbucket](https://bitbucket.com)

All the 3 of them are free to sign-up and their working plan might change within time. 
For purposes of our course it doesn't matter to which you sign-up, yet it is significant to mentioned that this course project is managed over [GitLab](https://gitlab.com). We'll be providing examples from there, yet you are welcome to create at **any** those sites as well as create accounts on **all** of them.

Thus lets create accounts for each and everyone one of you

<!-- demo creating the account -->
<!-- demo creating repository -->
---

# Cloning a remote GitLab repository

At this point we have created account and first project.

Now lets clone it.  lets go to the page of your page clone it to different folder.
```sh
$  git clone  https://gitlab.com/code
```
or
```sh
$  git clone  https://gitlab.com/code /path/to/some/different/folder
```
There is a possibility to clone the code to designated folder, by adding path to that folder at the end of the line.

---
# Cloning a remote GitLab repository(cont.)

When cloning repository, you can either choose SSH or HTTPS protocol for git to transfer data to gitlab/gitlab/bitbucket and so on. 

<img src='../99_misc/.img/git-clone.png' alt=diagram style="border-radius:24px;float:right;width:600px;">

We'll go with HTTPS for now.

Copy the URL onto your clipboard, and then go back to your terminal. In your terminal
`git clone URL` where `URL` is the link that you have used.
Due to HTTPS being the protocol that we used, authentication is required, so enter your username and password when prompted while cloning. Now verify by typing in ls. You should see a `code` directory. Change into project folder and `ls` again. All of the files should be the same as in our original project. The last thing we'll do here is check our remotes.

`git remote -v` will show that remotes are already setup for you.

Now you're ready to contribute to the `code` on the new host. 

That's it ... no strings attached 😁

---

# Getting and Pushing to a remote repository

When creating a local repository, we'll eventually need to save it, so we `push` changes to remote repository. The Flow for that process would be as follows:
```sh
$ ~$ cd ~/Project/code
$ ~$ git init
$ ~$ git config user.name alex.schapelle
$ ~$ git config user.name alex@vaiolabs.com
$ ~$ git remote add origin https://gitlab.com/alex.schapelle/code.git
$ ~$ git add .
$ ~$ git commit -m "adding initial commit"
$ ~$ git push -u origin main
```

---


# Getting and Pushing to a remote repository (cont.)

Yet, We usually do not work on the project alone, meaning that not just us are cloning that same project others as well. This is what is called **collaborative work** on project. In case it was not clear, many people `push` code to our project, but we'll never know it until we update our code by `pull`ing it from remote server.

To do so, we can `cd` in project and run `git pull`. The command will pull all the other `push`es from other developers on to the shared branch that we are working on together.

Another point in regards to **collaborative work** : when more then one person is working on the branch, whether it is `master`/`main` or any other branch, it is best practice to run `git pull` every time, before one `push`es her/his code. In terms of flow it would look some thing like this:

```sh
$ ~$ cd ~/Project/code
$ ~$ git pull
$ ~$ echo "echo 'updating some code'" >> README.md
$ ~$ git add .
$ ~$ git commit -m "updating README.md"
$ ~$ git push # after remote is established there is no need to add -u origin main
```

--- 

# Getting and Pushing to a remote repository (cont.)

Same would be in case of branches:

```sh
$ ~$ cd ~/Project/code
$ ~$ git checkout -b somebranch
$ ~$ git pull -u origin main  
# in this case we are updating main branch, and its effect will be transferred to some branch
$ ~$ echo "echo 'updating some code'" >> README.md
$ ~$ git add .
$ ~$ git commit -m "updating README.md"
$ ~$ git push
```

---
# Using SSH keys for authentication 

As mentioned in previous chapter, we have used `HTTPS` protocol to transfer data to git server. Yet other options also exists, most common of them being  `SSH` protocol. The main reason for its use case being, by passing step authentication with **ssh-keys**.

In order to set it up we'll need to:
- Generate ssh-key with `ssh-keygen` command
- Copy public key to remote server
- Change ssh config in `~/Projects/code/.git/config` file
- Authorize the ssh-key digital finger-print

---

# Using SSH keys for authentication (cont.)

### Generate ssh-key with `ssh-keygen` command

Just run command below

```sh
$ ~$ssh-keygen -t rsa -b 2048 \
 # -t: type -b: byte size of encryption
```
The command above should prompt us with several question like password and location of ssh-keys, to which I'd to suggest to leave empty and press `Enter` key.
In case to which you already have default ssh-key names used for something else, I'd suggest to use the `Public` portion for the git authentication, or just generate file with different name.

- **[!] WARNING**: There are some cases when, using ssh-key with other names then `id_rsa.pub` might require you to update git commands environment variable `GIT_SSH_COMMAND`**every time you use `git push/pull`**

As an example:

```sh 
$ ~$ GIT_SSH_COMMAND="ssh -i ~/.ssh/my_key.pub" git clone example
```

---

# Using SSH keys for authentication (cont.)

### Copy public key to remote server
In case you are using gitlab, open your account and go to preferences

<img src='../99_misc/.img/gitlab-preferance.png'alt=diagram style="border-radius:24px;float:right;width:500px;>


In there choose `SSH Keys`
<img src='../99_misc/.img/gitlab-ssh-key.png'alt=diagram style="border-radius:24px;float:right;width:500px;)


And at key area paste the content of your `id_rsa.pub` file.
![](../99_misc/.img/key.png)

Save the change.

---

# Using SSH keys for authentication (cont.)
### Change ssh config in `~/Projects/code/.git/config` file

Before run `git push` command it is suggested to edit the project configuration file. Just go to your project file and  change `url` of remote server as from this:

```sh
[remote "origin"]
	url = https://gitlabs.com/alex.schapelle/code.git
```

to this:

```sh
[remote "origin"]
	url = git@gitlab.com:alex.schapelle/code.git
```

- **[!] WARNING**: Do NOT change any other file under `remote` config.

---

# Using SSH keys for authentication (cont.)

There are cases where we have multiple ssh keys as well  multiple users to be used with git. Due to use of ssh protocol, we have to configure out ssh client config file `~/.ssh/config` to use specific ssh key to our git ssh connection.

Here is a simple example to use:
```sh
vi ~/.ssh/config
Host gitlab.com
  User alex.schapelle@ast-science.com
  Identityfile ~/.ssh/id_rsa # or any name of file that you have generated previously
```

---

# Using SSH keys for authentication (cont.)
### Authorize the ssh-key digital finger-print

All is left to authorize the ssh key and to do so you only need to try and use `git push/pull` command.

---

# Summary
